from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.shortcuts import render
from django.views.generic import ListView, DetailView, DeleteView
from .models import MataKuliah
from .forms import MatkulForm
from django.urls import reverse_lazy

@login_required
def index(request):

    form = MatkulForm(request.POST or None)
    if form.is_valid():
        form.save()
        form = MatkulForm()
    
    context = {'form' : form}
    return render(request, 'story5/index.html', context)

# class AddMatkulView(CreateView):
#     model = MataKuliah
#     template_name = 'story5/index.html'
#     fields = '__all__'

@method_decorator(login_required(login_url='login'), name='dispatch')
class MatkulView(ListView):
    model = MataKuliah
    template_name = 'story5/list_matkul.html'

@method_decorator(login_required(login_url='login'), name='dispatch')
class MatkulDetailView(DetailView):
    model = MataKuliah
    template_name = 'story5/matkul_details.html'

@method_decorator(login_required(login_url='login'), name='dispatch')
class DeleteMatkulView(DeleteView):
    model = MataKuliah
    template_name = 'story5/delete_matkul.html'
    success_url = reverse_lazy('story5:list-matkul')