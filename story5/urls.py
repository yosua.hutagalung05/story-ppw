from django.urls import path
from . import views
from .views import MatkulView, MatkulDetailView, DeleteMatkulView

app_name = 'story5'

urlpatterns = [
    path('', views.index, name='index'),
    path('listmatkul/', MatkulView.as_view(), name='list-matkul'),
    path('matkul/<int:pk>/', MatkulDetailView.as_view(), name='matkul-details'),
    path('matkul/<int:pk>/delete', DeleteMatkulView.as_view(), name='delete-matkul'),
]