from django.db import models
from django.urls import reverse

# Create your models here.
class MataKuliah(models.Model):
    nama_matkul = models.CharField(max_length=255)
    dosen_pengajar = models.CharField(max_length=255)
    jumlah_sks = models.CharField(max_length=2)
    deskripsi = models.TextField(blank=True, null=True)
    semester = models.CharField(max_length=20)
    ruang_kelas = models.CharField(max_length=10)

    # def get_absolute_url(self):
    #     return reverse('story5:matkul-details', args=(str(self.id)))
