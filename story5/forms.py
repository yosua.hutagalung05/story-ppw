from django import forms
from .models import MataKuliah

class MatkulForm(forms.ModelForm):
    class Meta:
        model = MataKuliah
        fields = (
            'nama_matkul',
            'dosen_pengajar',
            'jumlah_sks',
            'deskripsi',
            'semester',
            'ruang_kelas'
        )
        widgets = {
            'nama_matkul': forms.TextInput(attrs={'class': 'form-control'}),
            'dosen_pengajar': forms.TextInput(attrs={'class': 'form-control'}),
            'jumlah_sks': forms.TextInput(attrs={'class': 'form-control'}),
            'deskripsi': forms.Textarea(attrs={'class': 'form-control'}),
            'semester': forms.TextInput(attrs={'class': 'form-control'}),
            'ruang_kelas': forms.TextInput(attrs={'class': 'form-control'}),
        }
        