from django.contrib.auth.decorators import login_required
from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'story3/index.html')

@login_required
def extras(request):
    return render(request, 'story3/extras.html')