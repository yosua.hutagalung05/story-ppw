from django import forms
from .models import Orang, Kegiatan

class OrangForm(forms.ModelForm):
    # nama = forms.CharField(max_length=255)
    class Meta:
        model = Orang
        fields = {
            'nama',
        }
        widgets = {
            'nama' : forms.TextInput(attrs={'class': 'form-control'}),
        }

class KegiatanForm(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = {
            'nama'
        }
        widgets = {
             'nama' : forms.TextInput(attrs={'class': 'form-control'}),
        }