from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    nama = models.CharField(max_length=255)
    def __str__(self):
        return self.nama

class Orang(models.Model):
    nama = models.CharField(max_length=255)
    kegiatan = models.ForeignKey(Kegiatan, null=True, on_delete=models.CASCADE)
    def __str__(self):
        return self.nama