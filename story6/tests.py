from django.contrib.auth import authenticate, get_user_model
from django.test import TestCase, Client
from .models import Kegiatan, Orang

class Story6Test(TestCase):
    
    def test_tostring_kegiatan_model(self):
        test_kegiatan = Kegiatan.objects.create(nama='Berdemo')
        self.assertIn('Berdemo', test_kegiatan.__str__())

    def test_tostring_orang_model(self):
        test_kegiatan = Kegiatan.objects.create(nama='Berdemo')
        test_orang = Orang.objects.create(nama='Mahasiswa', kegiatan=test_kegiatan)
        self.assertIn('Mahasiswa', test_orang.__str__())

    def test_index_method_view(self):
        client = Client()
        user = get_user_model().objects.create_user(username="testuser", password="testtest")
        user.save()
        client.login(username="testuser", password="testtest")

        response = client.get('/story6/')
        self.assertEqual(response.status_code, 200)

    def test_add_event_method_view_get(self):
        client = Client()
        user = get_user_model().objects.create_user(username="testuser", password="testtest")
        user.save()
        client.login(username="testuser", password="testtest")

        response = client.get('/story6/add_event/')
        self.assertEqual(response.status_code, 200)

    def test_add_event_method_view_post(self):
        client = Client()
        user = get_user_model().objects.create_user(username="testuser", password="testtest")
        user.save()
        client.login(username="testuser", password="testtest")

        client.post('/story6/add_event/', {'nama':'Berdemo'})
        response = client.get('/story6/')
        return_html = response.content.decode('utf8')
        self.assertIn('Berdemo', return_html)
    
    def test_register_method_view_get(self):
        client = Client()
        user = get_user_model().objects.create_user(username="testuser", password="testtest")
        user.save()
        client.login(username="testuser", password="testtest")

        test_kegiatan = Kegiatan.objects.create(nama='Berdemo')
        test_kegiatan.save()
        url_str = '/story6/register/' + str(test_kegiatan.pk) +'/'
        response = client.get(url_str)
        self.assertEqual(response.status_code, 200)
    
    def test_register_method_view_post(self):
        client = Client()
        user = get_user_model().objects.create_user(username="testuser", password="testtest")
        user.save()
        client.login(username="testuser", password="testtest")

        test_kegiatan = Kegiatan.objects.create(nama='Berdemo')
        test_kegiatan.save()
        url_str = '/story6/register/' + str(test_kegiatan.pk) +'/'
        client.post(url_str, {'nama':'Mahasiswa'})
        response = client.get('/story6/')
        return_html = response.content.decode('utf8')
        self.assertIn('Mahasiswa', return_html)