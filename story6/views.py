from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from .models import Kegiatan, Orang
from .forms import OrangForm, KegiatanForm
from django.http import HttpResponseRedirect

@login_required
def index(request):
    events = Kegiatan.objects.all()
    participants = Orang.objects.all()
    response = {
        'events' : events,
        'participants' : participants,
    }
    return render(request, 'story6/index.html', response)

@login_required
def add_event(request):
    form = KegiatanForm(request.POST or None)
    if (request.method == 'GET'):
        response = {
            'form' : form
        }
        return render(request, 'story6/add_event.html', response)
    elif (form.is_valid() and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/story6/')

@login_required
def register(request, pk):
    event = Kegiatan.objects.get(pk=pk)
    form = OrangForm(request.POST or None)
    if (request.method == 'GET'):
        response = {
            'form' : form,
        }
        return render(request, 'story6/register.html', response)
    elif (form.is_valid() and request.method == 'POST'):
        name = form.cleaned_data['nama']
        participant = Orang.objects.create(nama = name, kegiatan = event)
        participant.save()
        return HttpResponseRedirect('/story6/')