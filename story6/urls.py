from django.urls import path
from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.index, name='index'),
    path('add_event/', views.add_event, name='add_event'),
    path('register/<int:pk>/', views.register, name='register'),
]