from django.contrib.auth import get_user_model
from django.test import TestCase, Client

# Create your tests here.
class TestStory7(TestCase):
    def test_url_exists(self):
        client = Client()
        user = get_user_model().objects.create_user(username="testuser", password="testtest")
        user.save()
        client.login(username="testuser", password="testtest")

        response = client.get('/story7/')
        self.assertEqual(response.status_code, 200)
    
    def test_template_used(self):
        client = Client()
        user = get_user_model().objects.create_user(username="testuser", password="testtest")
        user.save()
        client.login(username="testuser", password="testtest")
        
        response = client.get('/story7/')
        self.assertTemplateUsed('index.html')