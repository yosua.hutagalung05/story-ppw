from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

@login_required
def index(request):
    return render(request, 'story8/index.html')

@login_required
def data_request(request):
    arg = request.GET['q']
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + arg + "&maxResults=20"
    data = json.loads(requests.get(url).content)

    return JsonResponse(data, safe=False)