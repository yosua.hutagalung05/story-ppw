from django.contrib.auth import get_user_model
from django.test import TestCase, Client

# Create your tests here.
class TestStory8(TestCase):
    def test_index_url_exists(self):
        client = Client()
        user = get_user_model().objects.create_user(username="testuser", password="testtest")
        user.save()
        client.login(username="testuser", password="testtest")

        response = client.get('/story8/')
        self.assertEqual(response.status_code, 200)
    
    def test_index_template_used(self):
        client = Client()
        user = get_user_model().objects.create_user(username="testuser", password="testtest")
        user.save()
        client.login(username="testuser", password="testtest")

        response = client.get('/story8/')
        self.assertTemplateUsed('index.html')
