from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib import messages
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect

def login_request(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        arg = request.GET.get('next')

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            if arg is not None:
                return redirect(arg)
            else:
                return redirect('story3:index')
                
        else:
            messages.info(request, "Username or password is incorrect.")

    return render(request, 'story9/login.html')

def signup_request(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "New account successfully created.")
            return redirect('story9:login')

    elif request.method == 'GET':
        form = UserCreationForm()
    return render(request, 'story9/signup.html', {'form': form})

def logout_request(request):
    logout(request)
    return redirect('story3:index')

