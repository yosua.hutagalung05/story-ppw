from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.test import TestCase, Client

class TestStory9(TestCase):
    def test_login_url_exists(self):
        response = Client().get('/story9/login/')
        
        self.assertEqual(response.status_code, 200)

    def test_login_template_used(self):
        response = Client().get('/story9/login/')
        
        self.assertTemplateUsed('login.html')
    
    def test_signup_url_exists(self):
        response = Client().get('/story9/signup/')
        
        self.assertEqual(response.status_code, 200)
    
    def test_signup_template_used(self):
        response = Client().get('/story9/signup/')
        
        self.assertTemplateUsed('signup.html')
    
    def test_logout_url_redirected(self):
        response = Client().get('/story9/logout/')
        
        self.assertEqual(response.status_code, 302)
    
    def test_login_page_user_exist(self):
        client = Client()
        user = get_user_model().objects.create_user(username="testuser", password="testtest")
        user.save()
        client.post('/story9/login/', {'username':'testuser', 'password':'testtest'})
        
        self.assertTrue(user.is_authenticated)
    
    def test_login_page_user_doesnt_exist(self):
        response = Client().post('/story9/login/', {'username':'testuser', 'password':'testtest'})
        return_html = response.content.decode('utf8')
        
        self.assertIn('Username or password is incorrect.', return_html)
    
    def test_login_redirection_after_accessing_other_app(self):
        client = Client()
        user = get_user_model().objects.create_user(username="testuser", password="testtest")
        user.save()

        response = client.post('/story9/login/?next=/story5/', {'username':'testuser', 'password':'testtest'})
        
        self.assertEqual(response.status_code, 302)

    def test_signup(self):
        client = Client()
        client.post('/story9/signup/', {'username':'testuser', 'password1':'testtest12', 'password2':'testtest12'})
        
        self.assertEqual(User.objects.all().count(), 1)

